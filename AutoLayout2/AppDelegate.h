//
//  AppDelegate.h
//  AutoLayout2
//
//  Created by Mark Joselli on 3/3/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

