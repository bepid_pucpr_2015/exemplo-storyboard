//
//  ViewController.m
//  AutoLayout2
//
//  Created by Mark Joselli on 3/3/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)CloseView:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
